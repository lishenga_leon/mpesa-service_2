<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MpesaController extends Controller
{
    //
    protected $mpesa;


    public function __construct(){

        $this->mpesa = new \Safaricom\Mpesa\Mpesa();
    }


    public function doB2CPayment(Request $request){

        $InitiatorName = env('MPESA_INITIATOR_NAME'); 
        $PartyA = env('MPESA_SHORT_CODE');
        $CommandID = "BusinessPayment";
        $Amount = $request->input('amount');
        $PartyB = $request->input('phone_number');
        $Remarks = "please";
        $QueueTimeOutURL = url('api/logTimeout');
        $ResultURL = url('api/captureResults');
        $SecurityCredential = self::getSecurityCredentials();

        $b2cTransaction = $this->mpesa->b2c(
            $InitiatorName,
            $SecurityCredential, 
            $CommandID, 
            $Amount, 
            $PartyA,
            $PartyB, 
            $Remarks, 
            $QueueTimeOutURL, 
            $ResultURL, 
            $Occasion ?? ''
        );

        return $b2cTransaction;
    } 



    public function viewTransactionStatus(){
        $InitiatorName = env('MPESA_INITIATOR_NAME'); 
        $PartyA = env('MPESA_SHORT_CODE');
        $CommandID = "AccountBalance";
        $IdentifierType = "Shortcode";
        $TransactionID ="";
        $Remarks = "please";
        $QueueTimeOutURL = url('api/logTimeout');
        $ResultURL = url('api/captureResults');
        $SecurityCredential = self::getSecurityCredentials();
        $trasactionStatus=$this->mpesa->transactionStatus(
            $Initiator, 
            $SecurityCredential, 
            $CommandID, 
            $TransactionID, 
            $PartyA, 
            $IdentifierType, 
            $ResultURL, 
            $QueueTimeOutURL, 
            $Remarks, 
            $Occasion
        );
    }


    public function viewAccountBalance(){
        $InitiatorName = env('MPESA_INITIATOR_NAME'); 
        $PartyA = env('MPESA_SHORT_CODE');
        $CommandID = "AccountBalance";
        $IdentifierType = "4";
        $Remarks = "please";
        $QueueTimeOutURL = url('api/logTimeout');
        $ResultURL = url('api/captureResults');
        $SecurityCredential = self::getSecurityCredentials();
        $balanceInquiry = $this->mpesa->accountBalance(
                $CommandID, 
                $InitiatorName,
                $SecurityCredential, 
                $PartyA, 
                $IdentifierType, 
                $Remarks, 
                $QueueTimeOutURL, 
                $ResultURL
            );
        return $balanceInquiry;
    }


    public function captureResults(Request $request){
        \Log::info("-----Result----");
        \Log::info($request->all());
    }

    public function logTimeout(Request $request){
        \Log::info("-----Error----");
        \Log::info($request->all());
    }


    /***
     * @return
     */
    public static function getSecurityCredentials(){
        $publicKey = \Storage::disk('local')->get('cert.cer');
        $plaintext = env('MPESA_PASSWORD');

        openssl_public_encrypt($plaintext, $encrypted, $publicKey, OPENSSL_PKCS1_PADDING);

        return base64_encode($encrypted);

    }
}
